#
# Cookbook Name:: wkhtmltopdf
# Recipe:: default
#
#
# Depends on -- zlib, fontconfig, freetype, X11 libs (libX11, libXext, libXrender)
#
# http://download.gna.org/wkhtmltopdf/0.12/0.12.3/wkhtmltox-0.12.3_linux-generic-amd64.tar.xz
#
# SHA 256 40bc014d0754ea44bb90e733f03e7c92862f7445ef581e3599ecc00711dddcaa *wkhtmltox-0.12.3_linux-generic-amd64.tar.xz
#
#
# after untar get wkthmltox
# -- put that into /usr/local/lib
#
# ln /usr/local/lib/wkhtmltox/bin/wkhtmltopdf /usr/local/bin/wkhtmltopdf
wkhtmltopdf_version_folder =  '0.12'
wkhtmltopdf_version =  '0.12.3'
wkhtml_source = "wkhtmltox-#{wkhtmltopdf_version}_linux-generic-amd64.tar.xz"
wkhtmltopdf_url = "http://download.gna.org/wkhtmltopdf/#{wkhtmltopdf_version_folder}/#{wkhtmltopdf_version}/#{wkhtml_source}"
wkhtml_source_directory = 'wkhtml-source'
bin_path = '/usr/local/bin'
if ['solo', 'app', 'app_master', 'util'].include?(node[:instance_role])

  remote_file "/data/#{wkhtml_source}" do
    source "#{wkhtmltopdf_url}"
    owner node[:owner_name]
    group node[:owner_name]
    mode 0644
    backup 0
    not_if { FileTest.exists?("/data/#{wkhtml_source_directory}") }
  end

  execute "unarchive wkhtmltopdf source" do
    command "cd /data && tar xf #{wkhtml_source} && sync"
    not_if { FileTest.directory?("/data/#{wkhtml_source_directory}") }
  end

  execute "copy /data/wkhtmltox/bin/wkhtmltopdf to #{bin_path}" do
    command "cp /data/wkhtmltox/bin/wkhtmltopdf #{bin_path}"
  end

  execute "chmod wkhtmltopdf" do
    command "chmod 755 #{bin_path}/wkhtmltopdf"
  end

  packages = %w(zlib fontconfig freetype libX11 libXext libXrender)
  packages.each do |pkg|
    package pkg do
      action :install
    end
  end
end
